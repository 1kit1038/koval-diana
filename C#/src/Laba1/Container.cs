﻿namespace Koval01
{
    public class Container
    {
        public int Size { get; set; }
        private Student[] _students;

        public Container()
        {
            Size = 0;
            _students = new Student[0];
        }

        public void AddStudent(Student newStudent)
        {
            var tmpStudentArray = new Student[Size + 1];

            for (int i = 0; i < Size; i++)
            {
                tmpStudentArray[i] = _students[i];
            }

            tmpStudentArray[Size] = newStudent;

            _students = tmpStudentArray;
            Size++;
        }
        public void ShowAll()
        {
            foreach (var i in _students)
            {
                Entry.outputStudent(i);
            }
        }

    }
}
