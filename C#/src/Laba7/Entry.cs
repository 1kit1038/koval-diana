﻿using System;

namespace koval07
{
    public static class Entry
    {
        public static Student InputStudent()
        {
            int day;
            int month;
            int year;

            Console.Write("Last name: ");
            string newLastName = InputName();

            Console.Write("First name: ");
            string newName = InputName();

            Console.Write("Father`s name: ");
            string newFName = InputName();

            Console.Write("BirthDay: ");
            day = InputInt();
            Console.Write("BirthMonth: ");
            month = InputInt();
            Console.Write("BirthYear: ");
            year = InputInt();
            DateTime newBd = new DateTime(year, month, day);

            Console.Write("Date of enter: ");
            day = InputInt();
            Console.Write("Month of enter: ");
            month = InputInt();
            Console.Write("Year of enter: ");
            year = InputInt();
            DateTime newDateEnter = new DateTime(year, month, day);

            Console.Write("Group: ");
            string newNameGroup = InputString();

            Console.Write("Group index: ");
            char newGroupIndex = InputChar();

            Console.Write("Specialization: ");
            string newSpec = InputString();

            Console.Write("Performance: ");
            double newPerf = inputDouble();

            var tmpStudent = new Student(newNameGroup,
            newGroupIndex, newSpec, newName, newLastName, newFName, newBd,
            newDateEnter, newPerf);

            return tmpStudent;
        }

        public static string InputName()
        {
            string name = Console.ReadLine();

            while (!Check.ValidateName(name))
            {
                Console.WriteLine("This info is incorrect.");
                name = Console.ReadLine();
            }

            return name;
        }

        public static int InputInt()
        {
            while (true)
            {
                try
                {
                    return Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static double inputDouble()
        {
            while (true)
            {
                try
                {
                    double value = Convert.ToDouble(Console.ReadLine());
                    return value;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static char InputChar()
        {
            while (true)
            {
                try
                {
                    return Convert.ToChar(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }


        public static string InputString()
        {
            string sentence = Console.ReadLine();

            while (!Check.ValidateChar(sentence))
            {
                Console.WriteLine("This info is incorrect.");
                sentence = Console.ReadLine();
            }

            return sentence;
        }
    }
}

