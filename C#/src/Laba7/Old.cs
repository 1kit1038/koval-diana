﻿using System;
using System.Collections.Generic;
using System.Text;

namespace koval07
{
    public class Old
    {
        public int Year { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public Old()
        {

        }
        public override string ToString()
        {
            return Year + " years " + Month + " months " + Day + " days";
        }
    }
}
