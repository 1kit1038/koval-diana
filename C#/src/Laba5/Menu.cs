﻿using System;
using System.IO;
using System.Text;

namespace koval05

{
    public class Menu
    {
        private Container<Student> _list;
        delegate void Message();
        public void Start()
        {
            _list = new Container<Student>();
            string choice = "a";
            Message mes = printMenu;
            Student tmpStudent;

            while (choice != "0")
            {

                mes();
                Console.Write("Your choice: ");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Console.Clear();
                        tmpStudent = Entry.InputStudent();
                        _list.Add(tmpStudent);
                        Console.Clear();
                        break;
                    case "2":
                        Console.Clear();
                        ShowAll();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "3":
                        Console.Clear();
                        printByIndex();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "4":
                        Console.Clear();
                        WriteToFile();
                        Console.WriteLine("Info was written");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "5":
                        Console.Clear();
                        ReadFromFile();
                        Console.WriteLine("List was downloaded");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "6":
                        Console.Clear();
                        RedactStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "7":
                        Console.Clear();
                        int index;
                        Console.Write("Choose the index of student: ");
                        index = Convert.ToInt32(Console.ReadLine());
                        if (index > _list.Count() || index < 0)
                        {
                            Console.Write("Invalid index ");
                        }
                        else
                        {
                            ShowStudentSpecialization(index);
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "8":
                        Console.Clear();
                        int i;
                        Console.Write("Choose the index of student: ");
                        i = Convert.ToInt32(Console.ReadLine());
                        if (i > _list.Count() || i < 0)
                        {
                            Console.Write("Invalid index ");
                        }
                        else
                        {
                            ShowCourseAndSemestr(i);
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "9":
                        Console.Clear();
                        _list.Save();
                        break;
                    case "10":
                        Console.Clear();
                        _list.Download();
                        break;
                    case "11":
                        Console.Clear();
                        Console.WriteLine("1 - average age");
                        Console.WriteLine("2 - average rating");
                        Console.Write("Your choice: ");
                        int choice1 = Convert.ToInt32(Console.ReadLine());
                        if (choice1 == 1)
                        {
                            Console.WriteLine("1 - in group");
                            Console.WriteLine("2 - in specialization");
                            Console.WriteLine("3 - in faculty");
                            Console.Write("Your choice: ");
                            int choice2 = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Average age: " + AverageAge(choice2));
                        }
                        if (choice1 == 2)
                        {
                            Console.WriteLine("1 - in group");
                            Console.WriteLine("2 - in specialization");
                            Console.WriteLine("3 - in faculty");
                            Console.Write("Your choice: ");
                            int choice2 = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Average rating: " + AveragePerfomance(choice2));
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "12":
                        Console.Clear();
                        Console.Write("Set index: ");
                        index = Convert.ToInt32(Console.ReadLine());
                        RemoveByIndex(index);
                        Console.ReadLine();
                        Console.Clear();
                        break;
                }
            }
        }

        private static void printMenu()
        {
            Console.WriteLine("------Menu------\n");
            Console.WriteLine("1 - Add new student");
            Console.WriteLine("2 - Show data for all students");
            Console.WriteLine("3 - Print by index");
            Console.WriteLine("4 - Write to file");
            Console.WriteLine("5 - Read from file");
            Console.WriteLine("6 - Change info about students");
            Console.WriteLine("7 - Show student specialization");
            Console.WriteLine("8 - Show course and semestr");
            Console.WriteLine("9 - Save to xml");
            Console.WriteLine("10 - Download from xml");
            Console.WriteLine("11 - Show average perfomance or age");
            Console.WriteLine("12 - Remove by index");
            Console.WriteLine("0 - Exit");
        }

        public void ShowAll()
        {
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("| Index |          Name        |    Birthday    |             Age           |   Enter Date   | Group |     Group index   |    Specialization    | Performance |");
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
            string str;
            string[] infoStudent;
            var i = 0;
            foreach (var student in _list)
            {
                str = student.ToString();
                infoStudent = str.Split(new char[] { '|' });
                String output = String.Format("|{0,-7}|{1,-22}|{2,-16}|{3,-27}|{4,-16}|{5,-7}|{6,-19}|{7,-22}|{8,-13}|",
                    i, student.LastName + ' ' + student.FirstName + ' ' + student.Fathersname, student.Birthday.ToString("dd.MM.yyyy"), student.Age, student.DateEnter.ToString("dd.MM.yyyy"), student.NameOfGroup, student.Index_of_group, student.Specialization, student._perform);
                Console.WriteLine(output);
                i++;
            }

        }

        public void printByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            Console.WriteLine(_list[index]);
        }

        public void WriteToFile()
        {
            var path = @"C:\Users\Lenovo\Desktop\test.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    foreach (var student in _list)
                    {
                        sw.WriteLine(student.LastName + '/' + student.FirstName + '/' + student.Fathersname + '/' +
                            student.Birthday.ToString("d") + '/' + student.DateEnter.ToString("d") + '/' + student.NameOfGroup + '/' +
                            student.Index_of_group + '/' + student.Specialization + '/' + student._perform);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ReadFromFile()
        {
            string LastName;
            string FirstName;
            string Fathersname;
            DateTime Birthday;
            DateTime DateEnter;
            string NameOfGroup;
            char Index_of_group;
            string Specialization;
            double _perform;
            var path = @"C:\Users\Lenovo\Desktop\test.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split("/");

                        LastName = infoStudent[0];
                        FirstName = infoStudent[1];
                        Fathersname = infoStudent[2];
                        Birthday = DateTime.Parse(infoStudent[3]);
                        DateEnter = DateTime.Parse(infoStudent[4]);
                        NameOfGroup = infoStudent[5];
                        Index_of_group = char.Parse(infoStudent[6]);
                        Specialization = infoStudent[7];
                        _perform = int.Parse(infoStudent[8]);
                        var obj = new Student(NameOfGroup, Index_of_group, Specialization, FirstName, LastName, Fathersname, Birthday, DateEnter, _perform);
                        _list.Add(obj);

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RedactStudent()
        {
            if (_list.Count() == 0)
            {
                Console.WriteLine("The list of students is empty");
                Console.ReadLine();
                return;
            }
            string selection;
            var number = 0;
            var status = false;
            int index = 0;
            while (status == false)
            {
                Console.Write("Choose the index of student: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index < _list.Count() && index >= 0)
                {
                    status = true;
                }
            }

            Console.WriteLine("1 - exit");
            Console.WriteLine("You can change: ");
            Console.WriteLine("2 - Last name");
            Console.WriteLine("3 - First name");
            Console.WriteLine("4 - Father`s name");
            Console.WriteLine("5 - Birthday");
            Console.WriteLine("6 - Date of enter");
            Console.WriteLine("7 - Group");
            Console.WriteLine("8 - Group index");
            Console.WriteLine("9 - Specialization");
            Console.WriteLine("10 - Performance");
            selection = Console.ReadLine();
            int day;
            int month;
            int year;

            if (int.TryParse(selection, out number))
            {

                switch (number)
                {
                    case 1:

                        return;

                    case 2:

                        Console.Write("Last name: ");
                        string newLastName = Entry.InputName();
                        _list[index].LastName = newLastName;
                        break;

                    case 3:

                        Console.Write("First name: ");
                        string newName = Entry.InputName();
                        _list[index].FirstName = newName;

                        break;

                    case 4:

                        Console.Write("Father`s name: ");
                        string newFName = Entry.InputName();
                        _list[index].Fathersname = newFName;
                        break;

                    case 5:

                        Console.Write("BirthDay: ");
                        day = Entry.InputInt();
                        Console.Write("BirthMonth: ");
                        month = Entry.InputInt();
                        Console.Write("BirthYear: ");
                        year = Entry.InputInt();
                        DateTime newBd = new DateTime(year, month, day);
                        _list[index].Birthday = newBd;
                        break;

                    case 6:

                        Console.Write("Date of enter: ");
                        day = Entry.InputInt();
                        Console.Write("Month of enter: ");
                        month = Entry.InputInt();
                        Console.Write("Year of enter: ");
                        year = Entry.InputInt();
                        DateTime newDateEnter = new DateTime(year, month, day);
                        _list[index].DateEnter = newDateEnter;
                        break;

                    case 7:

                        Console.Write("Group: ");
                        string newNameGroup = Entry.InputString();
                        _list[index].NameOfGroup = newNameGroup;
                        break;

                    case 8:

                        Console.Write("Group index: ");
                        char newGroupIndex = Entry.InputChar();
                        _list[index].Index_of_group = newGroupIndex;
                        break;

                    case 9:

                        Console.Write("Specialization: ");
                        string newSpec = Entry.InputString();
                        _list[index].Specialization = newSpec;
                        break;

                    case 10:

                        Console.Write("Performance: ");
                        double newPerf = Entry.inputDouble();
                        _list[index]._perform = newPerf;
                        break;
                }

            }
        }

        public void ShowStudentSpecialization(int index)
        {
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            var info = new StringBuilder();
            info.Append($"Specialization: {_list[index].Specialization}\nName of group: {_list[index].NameOfGroup}\n" +
                $"Enter year: {_list[index].DateEnter.Year}\nGroup index: {_list[index].Index_of_group}\n");
            Console.Write(info);
        }

        public void ShowCourseAndSemestr(int index)
        {
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            var nowDate = DateTime.Now;
            int semester;

            int course;

            if (nowDate.Month >= 7)
            {
                course = nowDate.Year - _list[index].DateEnter.Year + 1;
            }
            else
            {
                course = nowDate.Year - _list[index].DateEnter.Year;
            }

            if (nowDate.Month >= 7)
            {
                semester = course * 2 - 1;
            }
            else
            {
                semester = course * 2;
            }
            var info = new StringBuilder();
            info.Append($"Student: {_list[index].LastName} {_list[index].FirstName} {_list[index].Fathersname}\n" +
                $"Course {course}, semester {semester}\n");
            Console.Write(info);
        }
        public int AveragePerfomance(int number)
        {
            int count = 0, progress = 0, av = 1;
            string str;

            switch (number)
            {
                case 1:
                    for (int i = 4; i > 0; i--)
                    {
                        Console.Write("Enter index of group: ");
                        str = Console.ReadLine();
                        if (str.Length > 1)
                        {
                            Console.Write("Invalid index of group. You have " + (i - 1) + " attempts");
                            if (i == 1)
                            {
                                return 0;
                            }
                        }
                        else
                        {
                            for (int j = 0; j < _list.Count(); j++)
                            {
                                if (_list[j].Index_of_group.Equals(Convert.ToChar(str)))
                                {
                                    count++;
                                    progress += (int)_list[j]._perform;
                                }
                            }
                            break;
                        }
                    }
                    break;

                case 2:
                    Console.Write("Enter specialization: ");
                    str = Console.ReadLine();
                    for (int i = 0; i < _list.Count(); i++)
                    {
                        if (_list[i].Specialization.Equals(str))
                        {
                            count++;
                            progress += (int)_list[i]._perform;
                        }
                    }
                    break;

                case 3:
                    Console.Write("Enter name of group: ");
                    str = Console.ReadLine();
                    for (int i = 0; i < _list.Count(); i++)
                    {
                        if (_list[i].NameOfGroup.Equals(str))
                        {
                            count++;
                            progress += (int)_list[i]._perform;
                        }
                    }
                    break;

            }
            av = progress / count;
            return av;
        }

        public int AverageAge(int number)
        {
            int count = 0, age = 0, av = 1;
            string str;
            switch (number)
            {
                case 1:
                    for (int i = 4; i > 0; i--)
                    {
                        Console.Write("Enter index of group: ");
                        str = Console.ReadLine();
                        if (str.Length > 1)
                        {
                            Console.Write("Invalid index of group. You have " + (i - 1) + " attempts");
                            if (i == 1)
                            {
                                return 0;
                            }
                        }
                        else
                        {
                            for (int j = 0; j < _list.Count(); j++)
                            {
                                if (_list[j].Index_of_group.Equals(Convert.ToChar(str)))
                                {
                                    count++;
                                    age += _list[j].Age.Year;
                                }
                            }
                            break;
                        }
                    }

                    break;
                case 2:
                    Console.Write("Enter specialization: ");
                    str = Console.ReadLine();
                    for (int i = 0; i < _list.Count(); i++)
                    {
                        if (_list[i].Specialization.Equals(str))
                        {
                            count++;
                            age += _list[i].Age.Year;
                        }
                    }

                    break;
                case 3:
                    Console.Write("Enter name of group: ");
                    str = Console.ReadLine();
                    for (int i = 0; i < _list.Count(); i++)
                    {
                        if (_list[i].NameOfGroup.Equals(str))
                        {
                            count++;
                            age += _list[i].Age.Year;
                        }
                    }

                    break;
            }

            av = age / count;
            return av;
        }

        void RemoveByIndex(int index)
        {
            _list.Remove(index);
        }

    }
}
