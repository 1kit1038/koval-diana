﻿using System.Text.RegularExpressions;

namespace koval05
{
    public static class Check
    {
        public const string NamePattern = @"^([A-Z])([a-z])*$";
        //public const string CharSimbol = @"^([a-z])$";
        public const string WordsPattern = @"[^\d\W]+\b";

        public static bool ValidateName(string name)
        {
            return Regex.IsMatch(name.Trim(), NamePattern);
        }

        public static bool ValidateChar(string value)
        {
            return Regex.IsMatch(value.Trim(), WordsPattern);
        }

        public static bool ValidateInt(int min, int max, int value)
        {
            return value >= min && value <= max;
        }
    }
}
