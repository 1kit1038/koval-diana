﻿using System;
using System.IO;

namespace koval03
{
    public class Menu
    {
        private Container<Student> _list;
        public void Start()
        {
            _list = new Container<Student>();
            char choice = 'a';

            Student tmpStudent;

            while (choice != '0')
            {

                printMenu();
                Console.Write("Your choice: ");
                choice = Entry.InputChar();

                switch (choice)
                {
                    case '1':
                        Console.Clear();
                        tmpStudent = Entry.InputStudent();
                        _list.Add(tmpStudent);
                        Console.Clear();
                        break;
                    case '2':
                        Console.Clear();
                        ShowAll();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '3':
                        Console.Clear();
                        printByIndex();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '4':
                        int index;
                        Console.Clear();
                        Console.Write("Set index: ");
                        index = Convert.ToInt32(Console.ReadLine());
                        RemoveByIndex(index);
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '5':
                        Console.Clear();
                        WriteToFile();
                        Console.WriteLine("Info was written");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '6':
                        Console.Clear();
                        ReadFromFile();
                        Console.WriteLine("List was downloaded");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '7':
                        Console.Clear();
                        RedactStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                }
            }
        }

        private static void printMenu()
        {
            Console.WriteLine("------Menu------\n");
            Console.WriteLine("1 - Add new student");
            Console.WriteLine("2 - Show data for all students");
            Console.WriteLine("3 - Print by index");
            Console.WriteLine("4 - Remove by index");
            Console.WriteLine("5 - Write to file");
            Console.WriteLine("6 - Read from file");
            Console.WriteLine("7 - Change info about students");
            Console.WriteLine("0 - Exit");
        }

        public void ShowAll()
        {
            int index = 0;
            foreach (var i in _list)
            {
                Console.WriteLine("Index of student: " + index);
                Console.WriteLine(i);
                index++;
            }
        }

        public void printByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            Console.WriteLine(_list[index]);
        }

        void RemoveByIndex(int index)
        {
            _list.Remove(index);
        }
        public void WriteToFile()
        {
            var path = @"C:\Users\Lenovo\Desktop\test.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    foreach (var student in _list)
                    {
                        sw.WriteLine(student.LastName + '/' + student.FirstName + '/' + student.Fathersname + '/' +
                            student.Birthday.ToString("d") + '/' + student.DateEnter.ToString("d") + '/' + student.NameOfGroup + '/' +
                            student.Index_of_group + '/' + student.Specialization + '/' + student._perform);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ReadFromFile()
        {
            var path = @"C:\Users\Lenovo\Desktop\test.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split("/");
                        var obj = new Student();
                        obj.LastName = infoStudent[0];
                        obj.FirstName = infoStudent[1];
                        obj.Fathersname = infoStudent[2];
                        obj.Birthday = DateTime.Parse(infoStudent[3]);
                        obj.DateEnter = DateTime.Parse(infoStudent[4]);
                        obj.NameOfGroup = infoStudent[5];
                        obj.Index_of_group = char.Parse(infoStudent[6]);
                        obj.Specialization = infoStudent[7];
                        obj._perform = int.Parse(infoStudent[8]);
                        _list.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RedactStudent()
        {
            if (_list.Count() == 0)
            {
                Console.WriteLine("The list of students is empty");
                Console.ReadLine();
                return;
            }
            string selection;
            var number = 0;
            var status = false;
            int index = 0;
            while (status == false)
            {
                Console.Write("Choose the index of student: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index < _list.Count() && index >= 0)
                {
                    status = true;
                }
            }

            Console.WriteLine("1 - exit");
            Console.WriteLine("You can change: ");
            Console.WriteLine("2 - Last name");
            Console.WriteLine("3 - First name");
            Console.WriteLine("4 - Father`s name");
            Console.WriteLine("5 - Birthday");
            Console.WriteLine("6 - Date of enter");
            Console.WriteLine("7 - Group");
            Console.WriteLine("8 - Group index");
            Console.WriteLine("9 - Specialization");
            Console.WriteLine("10 - Performance");
            selection = Console.ReadLine();
            int day;
            int month;
            int year;

            if (int.TryParse(selection, out number))
            {

                switch (number)
                {
                    case 1:

                        return;

                    case 2:

                        Console.Write("Last name: ");
                        string newLastName = Entry.InputName();
                        _list[index].LastName = newLastName;
                        break;

                    case 3:

                        Console.Write("First name: ");
                        string newName = Entry.InputName();
                        _list[index].FirstName = newName;

                        break;

                    case 4:

                        Console.Write("Father`s name: ");
                        string newFName = Entry.InputName();
                        _list[index].Fathersname = newFName;
                        break;

                    case 5:

                        Console.Write("BirthDay: ");
                        day = Entry.InputInt();                      
                        Console.Write("BirthMonth: ");
                        month = Entry.InputInt();
                        Console.Write("BirthYear: ");
                        year = Entry.InputInt();
                        DateTime newBd = new DateTime(year, month, day);
                        _list[index].Birthday = newBd;
                        break;

                    case 6:

                        Console.Write("Date of enter: ");
                        day = Entry.InputInt();
                        Console.Write("Month of enter: ");
                        month = Entry.InputInt();
                        Console.Write("Year of enter: ");
                        year = Entry.InputInt();
                        DateTime newDateEnter = new DateTime(year, month, day);
                        _list[index].DateEnter = newDateEnter;
                        break;

                    case 7:

                        Console.Write("Group: ");
                        string newNameGroup = Entry.InputString();
                        _list[index].NameOfGroup = newNameGroup;
                        break;

                    case 8:

                        Console.Write("Group index: ");
                        char newGroupIndex = Entry.InputChar();
                        _list[index].Index_of_group = newGroupIndex;
                        break;

                    case 9:

                        Console.Write("Specialization: ");
                        string newSpec = Entry.InputString();
                        _list[index].Specialization = newSpec;
                        break;

                    case 10:

                        Console.Write("Performance: ");
                        double newPerf = Entry.inputDouble();
                        _list[index]._perform = newPerf;
                        break;
                }

            }
        }

    }
}
