﻿using System;

namespace Laba2_.Net_
{
    public class Menu
    {
        private Container<Student> _list;
        public void Start()
        {
           _list = new Container<Student>();
            char choice= 'a';

            Student tmpStudent;

            while (choice != '0')
            {
                
                printMenu();
                Console.Write("Your choice: ");
                choice = Entry.InputChar();

                switch (choice)
                {
                    case '1':
                        Console.Clear();
                        tmpStudent = Entry.InputStudent();
                        _list.add(tmpStudent);
                        Console.Clear();
                        break;
                    case '2':
                        Console.Clear();
                        ShowAll();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '3':
                        Console.Clear();
                        printByIndex();
                        Console.ReadLine();
                        Console.Clear();
                        break;

                }
            }
        }

        private static void printMenu()
        {
            Console.WriteLine("------Menu------\n");
            Console.WriteLine("1 - Add new student");
            Console.WriteLine("2 - Show data for all students");
            Console.WriteLine("3 - Print by index");
            Console.WriteLine("0 - Exit");
        }

        public void ShowAll()
        {
            int index=0;
            foreach (var i in _list)
            {
                Console.WriteLine("Index of student: " + index );
                Console.WriteLine(i);
                index++;
            }
        }

        public void printByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            Console.WriteLine(_list[index]);
        }
        }
}
