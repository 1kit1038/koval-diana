﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laba2_.Net_
{
    public class Container<T>
    {
        private T[] _items;
        public Container()
        {
            _items = new T[0];
        }

        public int Count()
        {
            return _items.Length;
        }

        public void add(T newObj)
        {
            T[] _newItems = new T[_items.Length + 1];
            if (_items.Length > 0)
            {
                Array.Copy(_items, _newItems, _items.Length);
            }
            _newItems[_newItems.Length - 1] = newObj;
            _items = _newItems;
        }

        public T this[int index]
        {
            get
            {
                return _items[index];
            }

            set
            {
                _items[index] = value;
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            T[] _newItems = new T[_items.Length];
            Array.Copy(_items, _newItems, _items.Length);
            for (int i = 0; i < _items.Length; i++)
            {
                yield return _newItems[i];
            }
        }
    }
}
