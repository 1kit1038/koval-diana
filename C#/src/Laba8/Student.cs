﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Student
    {
        public string NameOfGroup { get; set; }
        public char Index_of_group { get; set; }
        public string Specialization { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fathersname { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime DateEnter { get; set; }
        public double _perform { get; set; }

        public Old Age { get; }

        public Student()
        {
            NameOfGroup = "CIT";
            Index_of_group = 'e';
            Specialization = "Computer engeneering";
            FirstName = "Jake";
            LastName = "Bobet";
            Fathersname = "Genriovich";
            Birthday = new DateTime(2001, 6, 6);
            DateEnter = new DateTime(2018, 6, 10);
            _perform = 100.0;
        }

        public Student(string newNameGroup, char newGroupIndex,
            string newSpec, string newName, string newLastName, string newFName,
            DateTime newBd, DateTime newDateEnter, double newPerf)
        {
            NameOfGroup = newNameGroup;
            Index_of_group = newGroupIndex;
            Specialization = newSpec;
            FirstName = newName;
            LastName = newLastName;
            Fathersname = newFName;
            Birthday = newBd;
            DateEnter = newDateEnter;
            _perform = newPerf;
            if (Birthday != null)
            {
                Age = new Old();
                var date = DateTime.Now;
                Age.Year = date.Year - Birthday.Year;
                if (date.Month >= Birthday.Month && date.Day >= Birthday.Day)
                {
                    Age.Month = date.Month - Birthday.Month;
                    Age.Day = date.Day - Birthday.Day;
                }
                else
                {
                    Age.Year -= 1;
                    Age.Month = date.Month + 11 - Birthday.Month;
                    Age.Day = DateTime.DaysInMonth(Birthday.Year, Birthday.Month) - Birthday.Day + date.Day;
                    if (Age.Day > DateTime.DaysInMonth(date.Year, date.Month))
                    {
                        Age.Month++;
                        Age.Day -= DateTime.DaysInMonth(date.Year, date.Month);
                    }
                }

            }
        }

        public override string ToString()
        {
            return $"Last name: {LastName}\nFirst name: {FirstName}\nFather`s name: {Fathersname}\n" +
                "BirthDate: " + Birthday.ToString("d") + "\n" +
                "EnterDate: " + DateEnter.ToString("d") + "\n" +
                "Group: " + NameOfGroup + "\n" +
                "Group Index: " + Index_of_group + "\n" +
                "Specialization: " + Specialization + "\n" +
                "Performance: " + _perform + "\n" + "Age of student: " + Age + "\n";
        }
    }

}

