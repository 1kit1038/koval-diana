﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1
{
    public class Old : Controller
    {
        public int Year { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public Old()
        {

        }
        public override string ToString()
        {
            return Year + " years " + Month + " months " + Day + " days";
        }
    }
}
