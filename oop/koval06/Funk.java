package ua.khpi.oop.koval06;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import ua.khpi.oop.kononenko05.Helper; //Импорт чужого класса

public class Funk {

    static Dop_class list = new Dop_class();

    public static void start() throws IOException, ClassNotFoundException {
        int choice;
        Scanner temp = new Scanner(System.in);

        do {
            Funk.print_main_menu();
            choice = temp.nextInt();
            switch (choice) {
                case 1:
                    Funk.get_data();
                    break;
                case 2:
                    Funk.print_arr();
                    break;
                case 3:
                    list.sortArray();
                    break;
                case 4:
                    Funk.addElement();
                    break;
                case 5:
                    Funk.removeElement();
                    break;
                case 6:
                    Scanner tempStr = new Scanner(System.in);
                    System.out.println("Which string do you need?");
                    String c = new String();
                    c = tempStr.nextLine();
                    System.out.println(list.indexOf(c));
                    break;
                case 7:
                    list.clear();
                    break;
                case 8:
                    Funk.saveContainer();
                    break;
                case 9:
                    Funk.recoverContainer();
                    break;
                case 10:
                    String temp2 = list.getByIndex(0);
                    Funk.FormatText(temp2);
                    break;
                case 11:
                    Helper a = new Helper(); //Использование чужого класса
                    boolean t= a.Polindrom( arg_1, arg_2);
                    System.out.println(t);
                    String temp3 = list.getByIndex(0);
                    a.Parse(temp3);
                    a.Print_result();
                    Helper.Print_info();
                    System.out.println("Press Any Key To Continue...");
                    new java.util.Scanner(System.in).nextLine();

                    break;
                default:
                    break;
            }
        } while (choice != 11);
    }


    static boolean Vowel(char c) {
        switch (c) {
            case 'A':
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                return true;
            default:
                return false;
        }
    }

    public static void FormatText(String text) {
        int strlenght = text.length();
        char b = ' ';
        StringBuilder s = new StringBuilder();

        int s1;
        int s2;

        m1:
        for (int i = 0; i < strlenght; i++) {
            char c = text.charAt(i);

            if (!(Vowel(c))) {
                s1 = i;
                while ((text.charAt(i) != ' ') && (i < strlenght)) {
                    i++;
                }
                s2 = i;
                if ((s2 - s1) == 6) {
                    continue m1;
                } else {
                    i = s1;
                    while ((text.charAt(i) != ' ') && (i < strlenght)) {
                        s.append(text.charAt(i));
                        i++;
                    }
                    s.append(' ');
                }
            } else {
                while ((text.charAt(i) != ' ') && (i < strlenght)) {
                    s.append(text.charAt(i));
                    i++;
                }
                s.append(' ');
            }

        }

        System.out.print("Result string: ");
        System.out.println(s);
    }

    public static void print_main_menu() {
        System.out.println("1.Enter data");
        System.out.println("2.Show current data");
        System.out.println("3.Sort by alphabet");
        System.out.println("4.Add data");
        System.out.println("5.Remove some element");
        System.out.println("6.Find index of element");
        System.out.println("7.Clear all");
        System.out.println("8.Save data");
        System.out.println("9.Recover data");
        System.out.println("10.FormatText");
        System.out.println("11.Exit");
    }

    public static void print_arr() {
        System.out.println("Your array is:");
        for (String str : list) {
            System.out.println("  " + str);
        }
    }

    public static void get_data() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the text:");
        String current = sc.nextLine();
        while (!current.equals("")) {
            list.add(new String(current));
            current = sc.nextLine();
        }
    }

    static public void addElement() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the text:");
        String current = sc.nextLine();
        list.add(current);
    }

    static public void removeElement() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the text:");
        String current = sc.nextLine();
        if (!list.remove(current)) System.out.println("Cannot find the string :(");
        ;
    }

    static public void saveContainer() throws IOException {
        FileOutputStream outputStream = new FileOutputStream("C:\\Users\\Lenovo\\IdeaProjects\\koval-diana\\src\\ua\\khpi\\oop\\koval06\\savedContainer.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(list);
        objectOutputStream.close();
    }

    static public void recoverContainer() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\Lenovo\\IdeaProjects\\koval-diana\\src\\ua\\khpi\\oop\\koval06\\savedContainer.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        list = (Dop_class) objectInputStream.readObject();
    }

}

