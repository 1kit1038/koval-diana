package ua.khpi.oop.koval01;

class Binary {

    public static void main(String[] args) {
        short s = 0x564;
        long l = 380981220767L;
        byte d = 0b1000011;
        int a = 05317;
        char symbol = 'G';

        System.out.println(s);
        System.out.println(l);
        System.out.println(d);
        System.out.println(a);
        System.out.println(symbol);
        Evenandodd(s);
        Evenandodd(d);
        Evenandodd(a);
        Evenandodd(symbol);
        Evenandodd(l);
        System.out.println("Numder s");
        Binaryform(s);
        System.out.printf("%n");
        System.out.println("Numder d");
        Binaryform(d);
        System.out.printf("%n");
        System.out.println("Numder a");
        Binaryform(a);
        System.out.printf("%n");
        System.out.println("Numder symbol");
        Binaryform(symbol);
        System.out.printf("%n");
        //For long type
        System.out.println("Numder l");
        Binaryform(l);
        System.out.printf("%n");


    }

    private static void printBinaryform(long number) {
        int i = 0;
        int t = 0;
        while (number != 0) {
            long temp = (number >> 1);
            temp = number - (temp << 1);

            if (temp == 1) {
                i++;
            } else {

                if (temp == 0) t++;
            }
            System.out.print(temp);
            number >>= 1;
        }

        System.out.println();
        System.out.print(" Kol of 1 in number " + i);
        System.out.println();
        System.out.print(" Kol of 0 in number " + t);
    }

    private static void Binaryform(long n) {
        if (n < 0) {
            System.out.println("Error: Not a positive integer");
        } else {

            System.out.print("Convert to binary is:");

            printBinaryform(n);

        }
    }


    private static void Evenandodd(long n) {
        long temp = n;
        int chet = 0;
        int nechet = 0;
        while (temp != 0) {

            if (temp % 2 == 0) {
                chet++;
            } else {
                nechet++;
            }
            temp /= 10;
        }
        System.out.println("Numder : " + n + " ood:" + chet + " even:" + nechet);

    }
}
