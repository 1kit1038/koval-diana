package ua.khpi.oop.koval07;

public class Funk {

    private String sex;
    private int registration_number;
    private String registration_date;
    private String name;
    private Info_about me;
    private Info_about preferencess;


    public Funk() {

    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(int registration_number) {
        this.registration_number = registration_number;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(String registration_date) {
        this.registration_date = registration_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Info_about getme() {
        return me;
    }

    public void setme(Info_about temp) {
        this.me = temp;
    }

    public Info_about getpreferencess() {
        return preferencess;
    }

    public void setpreferencess(Info_about temp) {
        this.preferencess = temp;
    }


    public class Info_about {
        private int height;
        private String eye_color;

        public int get_height() {
            return height;
        }

        public void set_height(int height) {
            this.height = height;
        }

        public String get_eye_color() {
            return eye_color;
        }

        public void set_eye_color(String eye_color) {
            this.eye_color = eye_color;
        }

        public void print() {
            System.out.printf("Height %d \nEye color %s", this.height, this.eye_color);
            System.out.println("");
        }
    }

    public void createInfoabout_me(int height, String eye_color) {
        me = new Info_about();
        me.set_height(height);
        me.set_eye_color(eye_color);
    }

    public void createInfoabout_somebody(int height, String eye_color) {
        preferencess = new Info_about();
        preferencess.set_height(height);
        preferencess.set_eye_color(eye_color);
    }

    public void print() {
        System.out.println("Info about candidats:");
        System.out.printf("Sex: %s \nRegistration number: %d \nRegistration_date: %s \nName: %s \n", this.sex, this.registration_number, this.registration_date, this.name);
        System.out.println("Dop info about this person: ");
        this.me.print();
        System.out.println("The requierments for a perfect match are: ");
        // System.out.println("Info about second person: ");
        this.preferencess.print();
    }
}