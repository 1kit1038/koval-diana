package ua.khpi.oop.koval07;

import java.util.ArrayList;
import java.util.Scanner;

public class Dop_class {

    public static void lets_go() {
        System.setProperty("console.encoding", "Cp866");
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the size of array:");
        int arraySize = sc.nextInt();
        ArrayList<Funk> person = new ArrayList<Funk>();
        Funk temp = null;
        for (int i = 0; i < arraySize; i++) {
            temp = new Funk();
            System.out.println("----------------" + (i + 1) + "----------------");
            temp.setName((enter_name()));
            temp.setRegistration_date((enter_registration_date()));
            temp.setRegistration_number((enter_registration_number()));
            temp.setSex((enterSex()));
            System.out.println("Enter dop info about this person: ");
            temp.createInfoabout_me(enter_height(), enter_eye_color());
            System.out.println("Enter dop info about perfect match: ");
            temp.createInfoabout_somebody(enter_height(), enter_eye_color());
            person.add(temp);
            temp = null;
        }
        showAccounts(person);
    }

    public static void showAccounts(ArrayList<Funk> person) {
        for (int i = 0; i < person.size(); i++) {
            person.get(i).print();
        }
    }

    public static String enterSex() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Sex: ");
        String sex = sc.nextLine();
        return sex;
    }

    public static String enter_name() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter name: ");
        String name = sc.nextLine();
        return name;
    }

    public static int enter_height() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter height: ");
        int height = sc.nextInt();
        return height;
    }

    public static int enter_registration_number() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter registration number : ");
        int registration_number = sc.nextInt();
        return registration_number;
    }

    public static String enter_registration_date() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter registration date : ");
        String registration_date = sc.nextLine();
        return registration_date;
    }

    public static String enter_eye_color() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter eye color : ");
        String eye_color = sc.nextLine();
        return eye_color;
    }


}

