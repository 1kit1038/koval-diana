package ua.khpi.oop.koval02;

import java.util.Random;

public class work {
    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            Random number = new Random();
            int n = 1523 + number.nextInt(2000);

            System.out.println("Таблица номер " + (i + 1) + " :");
            System.out.println(" _____________________________________________________ ");
            System.out.print("|Рандомное число из 4 цифр                  | ");
            System.out.println(n + "    |");
            System.out.println(" _____________________________________________________ ");
            System.out.print("|Значение данного числа в восьмеричном коде | ");
            funk(n);
            System.out.println(" _____________________________________________________ ");
        }
    }

    private static void funk(int n) {
        int k = n;
        int temp = 0;
        int temp1 = 0;
        int temp2 = 0;
        int temp3 = 0;
        int temp4 = 0;
        int i = 0;

        while (k != 0) {
            temp += (k % 8) * (int) Math.pow(10, i);
            i++;
            k /= 8;
        }

        System.out.println(temp + "    |");
        temp1 = temp % 10;
        temp /= 10;
        temp2 = temp % 10;
        temp /= 10;
        temp3 = temp % 10;
        temp /= 10;
        temp4 = temp % 10;

        if (temp1 == temp4) {
            System.out.println(" _____________________________________________________ ");
            System.out.print("|Числа первое и последнее равны                       |");
            System.out.println();
        } else {
            System.out.println(" _____________________________________________________ ");
            System.out.print("|Числа первое и последнее не совпали                  |");
            System.out.println();
        }

        if (temp2 == temp3) {
            System.out.println(" _____________________________________________________ ");
            System.out.print("|Числа второе и предпоследнее равны                   |");
            System.out.println();
        } else {
            System.out.println(" _____________________________________________________ ");
            System.out.print("|Числа второе и предпоследнее не совпали              |");
            System.out.println();
        }
    }
}


