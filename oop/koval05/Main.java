package ua.khpi.oop.koval05;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        String[] words = new String[7];
        Dop_class.get_data(words);
        // String text = " Sergeo and Georgy can swimming and dancing ";
        Funk test = new Funk(words);

        System.out.println(test.size());
        System.out.println(test.contains("Sergeo"));
        System.out.println(test.remove("Sergeo"));
        System.out.println(test.toString());
        // test.add("Bobbi")\
        // test.clear();

        Iterator<String> iter = test.iterator();

        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}