package ua.khpi.oop.koval05;

import java.util.Scanner;

public class Dop_class {

    public static void print_res_string(String outText) {
        System.out.print("Result string: ");
        System.out.println(outText);
        System.out.println("");
    }

    public static void print_arr(String[] arr) {

        System.out.println("Your array is:");
        for (int i = 0; i < 7; i++) {
            System.out.println(i + "." + arr[i]);
        }
        System.out.println("");
    }

    public static void get_data(String[] arr) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a couple of strings(7)");
        for (int i = 0; i < 7; i++) {
            arr[i] = scan.nextLine();
        }
    }

}


