package ua.khpi.oop.koval08;

import java.io.Serializable;
import java.util.GregorianCalendar;

public class Info_about_myself implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private int height;
    private String eye_color;
    private GregorianCalendar birthDate;
    private String hobby;

    public Info_about_myself() {
        name = new String();
        height = 0;
        eye_color = new String();
        birthDate = new GregorianCalendar();
        hobby = new String();

    }

    public Info_about_myself(String name, int height, String eye_color, GregorianCalendar birthDate, String hobby) {
        this.name = name;
        this.height = height;
        this.eye_color = eye_color;
        this.birthDate = birthDate;
        this.hobby = hobby;
    }

    public int get_height() {
        return height;
    }

    public void set_height(int height) {
        this.height = height;
    }

    public String get_eye_color() {
        return eye_color;
    }

    public void set_eye_color(String eye_color) {
        this.eye_color = eye_color;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public GregorianCalendar getBirthDate() {
        return birthDate;
    }


    public void setBirthDate(GregorianCalendar birthDate) {
        this.birthDate = birthDate;
    }


    public String getHobby() {
        return hobby;
    }


    public void setHobby(String hobby) {
        this.hobby = hobby;
    }


}
