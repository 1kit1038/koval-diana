package ua.khpi.oop.koval08;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Interface {

    public static void print_main_menu() {
        System.out.println("1.Enter data");
        System.out.println("2.Show current data");
        System.out.println("3.Clear all");
        System.out.println("4.Save or recover data");
        System.out.println("5.Exit");
    }

    public static void ShowPerson(ArrayList<Bureau> person) {
        for (int i = 0; i < person.size(); i++) {
            System.out.println("------------------" + (i) + "------------------");
            System.out.println("Sex: " + person.get(i).getSex());
            System.out.println("Registration number: " + person.get(i).getRegistration_number());
            System.out.println("Registration date: " + person.get(i).getRegistration_date().getTime());
            System.out.println("About yourself:");
            System.out.println("Name: " + person.get(i).getInfo().getName());
            System.out.println("Height: " + person.get(i).getInfo().get_height());
            System.out.println("Eye color: " + person.get(i).getInfo().get_eye_color());
            System.out.println("Date of birth: " + person.get(i).getInfo().getBirthDate().getTime());
            System.out.println("Hobby: " + person.get(i).getInfo().getHobby());
            System.out.println("Requirements:");
            int j = 0;
            for (String s : person.get(i).getRequirements()) {
                System.out.println((++j) + ") " + s);
            }
            System.out.println("------------------------------------");
        }
    }

    public static String enterSex() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Sex: ");
        String sex = sc.nextLine();
        return sex;
    }

    public static String enter_name() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter name: ");
        String name = sc.nextLine();
        return name;
    }

    public static int enter_height() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter height: ");
        int height = sc.nextInt();
        return height;
    }


    public static int enter_registration_number() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter registration number : ");
        int registration_number = sc.nextInt();
        return registration_number;
    }

    public static GregorianCalendar enter_registration_date() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter year: ");
        int year = sc.nextInt();
        System.out.print("Enter month: ");
        int month = sc.nextInt();
        System.out.print("Enter day: ");
        int day = sc.nextInt();
        return new GregorianCalendar(year, month - 1, day);
    }

    public static String enter_eye_color() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter eye color : ");
        String eye_color = sc.nextLine();
        return eye_color;
    }

    public static String enter_hobby() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter hobby: ");
        String hobby = sc.nextLine();
        return hobby;
    }

    public static ArrayList<Bureau> get_data() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);

        String sex = new String("1");
        int registration_number = 0;
        GregorianCalendar registration_date = new GregorianCalendar(1900, Calendar.JANUARY, 1);
        String name = new String("1");
        int height;
        String eye_color = new String("1");
        GregorianCalendar birthDate = new GregorianCalendar(1900, Calendar.JANUARY, 1);
        ;
        String hobby = new String("1");

        ArrayList<String> requirements = new ArrayList<String>();
        System.out.print("Enter the size of array:");
        int arraySize = sc.nextInt();
        ArrayList<Bureau> person = new ArrayList<Bureau>();


        for (int i = 0, j = 1; i < arraySize; i++, j++) {
            System.out.println("----------------" + (j) + "----------------");

            sex = (enterSex());
            registration_number = ((enter_registration_number()));
            System.out.println("\nEnter registration date:");
            registration_date = ((enter_registration_date()));

            name = ((enter_name()));
            height = (enter_height());
            eye_color = (enter_eye_color());
            System.out.println("\nEnter date of birth:");
            birthDate = (enter_registration_date());
            hobby = (enter_hobby());


            String enter = "";
            int k = 0;
            while (!"0".equals(enter)) {
                System.out.println("1 - Enter new requirement");
                System.out.println("0 - Exit");
                enter = sc.next();
                try {
                    k = Integer.parseInt(enter);
                } catch (NumberFormatException e) {
                    System.out.println("Incorrect data!");
                }

                switch (k) {
                    case 1:
                        System.out.println("Enter requirement:");
                        requirements.add(new Scanner(System.in).nextLine());
                        break;

                    case 0:
                        break;
                }

            }


            person.add(new Bureau(sex, registration_number, registration_date, new Info_about_myself(name, height, eye_color, birthDate, hobby), requirements));
        }
        return person;
    }


    public static void printXmlMenu() {

        System.out.println("1. Save database");
        System.out.println("2. Recover database");

    }

    public static void printFileChooseMenu() {

        System.out.println("1. Continue choosing ");
        System.out.println("2. Create file in current directory");
        System.out.println("3. Return to previous directory");
        System.out.println("0. Exit");

    }

    public static void cls() {

        for (int i = 0; i < 100; i++) {

            System.out.println();

        }

    }
}
