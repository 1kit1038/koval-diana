package ua.khpi.oop.koval08;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Bureau implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sex;
    private int registration_number;
    private GregorianCalendar registration_date;
    private Info_about_myself info;
    private ArrayList<String> requirements;

    public Bureau() {
        sex = new String();
        registration_number = 0;
        registration_date = new GregorianCalendar(1900, Calendar.JANUARY, 1);

    }

    public Bureau(String sex, int registration_number, GregorianCalendar registration_date, Info_about_myself info, ArrayList<String> requirements) {
        this.sex = sex;
        this.registration_number = registration_number;
        this.registration_date = registration_date;
        this.info = info;
        this.requirements = requirements;

    }


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(int registration_number) {
        this.registration_number = registration_number;
    }

    public GregorianCalendar getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(GregorianCalendar registration_date) {
        this.registration_date = registration_date;
    }


    public ArrayList<String> getRequirements() {
        return this.requirements;
    }

    public void setRequirements(ArrayList<String> requirements) {
        this.requirements = requirements;
    }


    public Info_about_myself getInfo() {
        return info;
    }


    public void setInfo(Info_about_myself info) {
        this.info = info;
    }
}

