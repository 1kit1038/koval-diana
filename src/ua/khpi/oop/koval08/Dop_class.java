package ua.khpi.oop.koval08;

import java.io.*;
import java.util.Scanner;

import java.util.ArrayList;


public class Dop_class {

    @SuppressWarnings("resource")
    public static void lets_go() throws FileNotFoundException {
        ArrayList<Bureau> person = new ArrayList<Bureau>();
        System.setProperty("console.encoding", "Cp866");
        int choice = 0;
        Scanner temp = new Scanner(System.in);
        do {
            Interface.print_main_menu();
            choice = temp.nextInt();
            switch (choice) {

                case 1:
                    person = Interface.get_data();
                    break;

                case 2:
                    System.out.println("----------------");
                    Interface.ShowPerson(person);
                    System.out.println("----------------");
                    System.out.println("Enter any key to continue...");
                    new java.util.Scanner(System.in).nextLine();
                    break;

                case 3:
                    person.clear();
                    break;

                case 4:
                    String xml = "";
                    int z = 0;
                    Interface.printXmlMenu();
                    String path = "C:\\Users\\Lenovo\\IdeaProjects\\koval-diana";
                    xml = temp.next();
                    try {
                        z = Integer.parseInt(xml);
                    } catch (NumberFormatException e) {
                        System.out.println("Incorrect input!");
                    }

                    switch (z) {

                        case 1:
                            String find = "";
                            int k = 0;
                            while (!"0".equals(find)) {
                                System.out.println("Current path: " + path);
                                Interface.printFileChooseMenu();
                                find = temp.next();
                                try {
                                    k = Integer.parseInt(find);
                                } catch (NumberFormatException e) {
                                    System.out.println("Incorrect input!");
                                }

                                switch (k) {

                                    case 1:

                                        path = Files.chooseFile(Files.getListOfFiles(path));
                                        break;

                                    case 2:

                                        path = Files.createFile(path);
                                        break;

                                    case 3:

                                        path = Files.moveHigher(path);
                                        break;

                                }

                            }


                            try {

                                Serialization.LongTermPersistenceWrite(person, path);

                            } catch (FileNotFoundException e) {

                                System.out.println("No such file!");
                                System.out.println("Enter any key to continue...");
                                new java.util.Scanner(System.in).nextLine();

                            }

                            break;

                        case 2:

                            String find2 = "";
                            int k2 = 0;
                            while (!"0".equals(find2)) {

                                System.out.println("Current path: " + path);
                                System.out.println("1. Continue choosing ");
                                System.out.println("2. Return to previous directory");
                                System.out.println("0. Exit");

                                find2 = temp.next();

                                try {

                                    k2 = Integer.parseInt(find2);
                                } catch (NumberFormatException e) {
                                    System.out.println("Incorrect input!");
                                }

                                switch (k2) {

                                    case 1:

                                        path = Files.chooseFile(Files.getListOfFiles(path));
                                        break;

                                    case 2:
                                        path = Files.moveHigher(path);
                                        break;
                                }
                            }

                            try {
                                person = Serialization.LongTermPersistenceRead(path);
                            } catch (FileNotFoundException e) {
                                System.out.println("No such file!");
                                System.out.println("Enter any key to continue...");
                                new java.util.Scanner(System.in).nextLine();
                            }


                            break;

                    }
                case 5:
                    System.out.println("Press Any Key To Continue...");
                    new java.util.Scanner(System.in).nextLine();

                    break;
            }
            System.out.println();
            // Interface.cls();
        } while (choice != 5);
        temp.close();
    }


}
