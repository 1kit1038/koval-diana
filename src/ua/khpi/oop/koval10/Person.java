package ua.khpi.oop.koval10;

import java.io.Serializable;
import java.util.Date;

public class Person implements Serializable{
    private static final long serialVersionUID = 1L;
    public int registration_number;
    public String sex;
    public Date dateOfRegistration;
    public String name;
    public int age;
    public String hobby;
    public String requirements;

    //-----------------------------------------------------------------------

    public int getRegistration_number() {
        return registration_number;
    }
    public String getSex() {
        return sex;
    }
    public String getDateOfRegistration() {
        return dateOfRegistration.toString();
    }
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
    public String getHobby() {
        return hobby;
    }
    public String getRequirements() {
        return requirements;
    }

    //-------------------------------------------------------------------

    public void setRegistration_number(int registration_number) {
        this.registration_number = registration_number;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }
}
