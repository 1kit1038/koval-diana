package ua.khpi.oop.koval10;


import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException {
        Bureau registration = new Bureau();
        for(String str : args) {
            if (str.equals("-auto")) {
                Interface.auto(registration);
                return;
            }
        }
        Interface.menu(registration);
    }
}