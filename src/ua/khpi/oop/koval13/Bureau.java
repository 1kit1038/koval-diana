package ua.khpi.oop.koval13;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Bureau implements Serializable{
    private static final long serialVersionUID = 1L;
    ArrayList<Person> person = new ArrayList<Person>();

    //------------------------------------------------------

    public void register(String gender, Date dateOfRegistration, String name, int age, String hobby, String requirements, String phoneNumber, String phoneProvider) {
        Person person = new Person();
        person.setRegistration_number(getRandomValue());
        person.setGender(gender);
        person.setDateOfRegistration(dateOfRegistration);
        person.setName(name);
        person.setAge(age);
        person.setHobby(hobby);
        person.setRequirements(requirements);
        person.setPhoneNum(phoneNumber);
        person.setPhoneProvider(phoneProvider);
        this.person.add(person);
    }


    public void print() {
        for(int i = 0; i < this.person.size(); i++) {
            printByIndex(i);
        }
    }

    public void printByIndex(int index) {
        System.out.println("Number of registration: " + this.person.get(index).getRegistration_number());
        System.out.println("Gender: " + this.person.get(index).getGender());
        System.out.println("Date of registration: " + this.person.get(index).getDateOfRegistration());
        System.out.println("Name: " + this.person.get(index).getName());
        System.out.println("Age: " + this.person.get(index).getAge());
        System.out.println("Hobby: " + this.person.get(index).getHobby());
        System.out.println("Requirements: " + this.person.get(index).getRequirements());
        System.out.println("Phone number: " + this.person.get(index).getPhoneNum() + " (" + this.person.get(index).getPhoneProvider() + ")" + "\n");
    }

    public void setPerson(Person person) {
        this.person.add(person);
    }

    public void clearClients() {
        this.person.clear();
    }

    public Person getClientByIndex(int index){
        return this.person.get(index);
    }

    public ArrayList<Person> getClients() {
        return this.person;
    }

    public int getSize() {
        return this.person.size();
    }

    private int getRandomValue() {
        int min = 0;
        int max = 99999999;
        int diff = max - min;
        Random random = new Random();
        int value = random.nextInt(diff + 1);
        return value += min;
    }
}