package ua.khpi.oop.koval13;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    static Bureau registration = new Bureau();
    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException, InterruptedException {

        for(String str : args) {
            if (str.equals("-auto")) {
                Interface.auto(registration);
                return;
            }
        }
        //Interface.menu(registration);
        Interface.auto(registration);
    }
}