package ua.khpi.oop.koval09;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.GregorianCalendar;

public class Person implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private int height;
    private String eye_color;
    private LocalDate birthDate;
    private String hobby;

    public Person() {

    }

    public Person(String name, int height, String eye_color, LocalDate birthDate, String hobby) {
        this.name = name;
        this.height = height;
        this.eye_color = eye_color;
        this.birthDate = birthDate;
        this.hobby = hobby;
    }

    public int get_height() {
        return height;
    }

    public void set_height(int height) {
        this.height = height;
    }

    public String get_eye_color() {
        return eye_color;
    }

    public void set_eye_color(String eye_color) {
        this.eye_color = eye_color;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public LocalDate getBirthDate() {
        return birthDate;
    }


    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }


    public String getHobby() {
        return hobby;
    }


    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String toString() {
        StringBuilder test = new StringBuilder("\nName: " + this.name +
                "\nDate of Birth: " + this.birthDate +
                "\nHeight:  " + this.height +
                "\nEye color: " + this.eye_color +
                "\nHobby: " + this.hobby + "\n");

        return test.toString();
    }
}
