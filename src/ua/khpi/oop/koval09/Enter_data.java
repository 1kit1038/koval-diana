package ua.khpi.oop.koval09;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;


public class Enter_data {

    public static void print_main_menu() {
        System.out.println("1.Enter data");
        System.out.println("2.Show current data");
        System.out.println("3.Clear all");
        System.out.println("4.Save or recover data");
        System.out.println("5.Exit");
    }


    public static Bureau insert() {
        Bureau output = new Bureau();
        Person lol = new Person();
        lol.setName(enter_name());
        output.setSex(enterSex());
        System.out.print("Enter the birthday(D/m/yyyy) : ");
        lol.setBirthDate(enter_registration_date());
        lol.set_height(enter_height());
        lol.set_eye_color(enter_eye_color());
        lol.setHobby(enter_hobby());

        output.setInfo(lol);

        output.setRegistration_number(enter_registration_number());
        System.out.print("Enter the registration date(D/m/yyyy) : ");
        output.setRegistration_date(enter_registration_date());
        output.setRequirements(enter_requirements());
        return output;
    }


    private static ArrayList<String> enter_requirements() {
        String tempStr;
        ArrayList<String> output = new ArrayList<>();
        Scanner in2 = new Scanner(System.in);

        System.out.print("Input requirements \n\t\t(use exit to stop adding): ");
        tempStr = in2.nextLine();
        while (!tempStr.equals("exit")) {
            output.add(tempStr);
            System.out.print("Input requirements \n\t\t(use exit to stop adding): ");
            tempStr = in2.nextLine();
        }
        return output;
    }

    public static String enterSex() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the gender: ");
        String sex = sc.nextLine();
        return sex;
    }

    public static String enter_name() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the name: ");
        String name = sc.nextLine();
        return name;
    }

    public static int enter_height() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the height: ");
        int height = sc.nextInt();
        return height;
    }


    public static int enter_registration_number() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the registration number : ");
        int registration_number = sc.nextInt();
        return registration_number;
    }

    public static LocalDate enter_registration_date() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("d/M/yyyy");
        LocalDate date = LocalDate.parse(sc.nextLine(), dateFormat);


        System.out.println(date);
        return date ;
    }

    public static String enter_eye_color() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the eye color : ");
        String eye_color = sc.nextLine();
        return eye_color;
    }

    public static String enter_hobby() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the hobby: ");
        String hobby = sc.nextLine();
        return hobby;
    }

}
