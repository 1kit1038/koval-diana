package ua.khpi.oop.koval09;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            Interface.starter(args);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Error");
            System.err.println(e.getMessage());
        }
    }

}