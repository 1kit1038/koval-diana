package ua.khpi.oop.koval09;

import java.beans.XMLEncoder;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;


public class Bureau implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sex;
    private int registration_number;
    private LocalDate registration_date;
    private Person info;
    private ArrayList<String> requirements;

    public Bureau() {

    }

    public Bureau(String sex, int registration_number, LocalDate registration_date, Person info, ArrayList<String> requirements) {
        this.sex = sex;
        this.registration_number = registration_number;
        this.registration_date = registration_date;
        this.info = info;
        this.requirements = requirements;

    }


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(int registration_number) {
        this.registration_number = registration_number;
    }

    public LocalDate getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(LocalDate registration_date) {
        this.registration_date = registration_date;
    }


    public ArrayList<String> getRequirements() {
        return this.requirements;
    }

    public void setRequirements(ArrayList<String> requirements) {
        this.requirements = requirements;
    }


    public Person getInfo() {
        return info;
    }


    public void setInfo(Person info) {
        this.info = info;
    }

    public String toString() {
        StringBuilder test = new StringBuilder("\nInfo:" + this.info +
                "\nSex: " + this.sex +
                "\nRegistration_date: " + this.registration_date +
                "\nRegistration_number:  " + this.registration_number +
                "\nRequirements: " + this.requirements + "\n");
        for (String s : requirements) {
            test.append( s + "\n");
        }
        return test.toString();
    }

    public void saveListXML(XMLEncoder encoder) {
        try {
            encoder.writeObject(requirements);
        } catch (Exception ignored) {

        }
    }
}

