package ua.khpi.oop.koval09;

import java.beans.*;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;


public class Serialization {


    public static void write(My_linked_list<Bureau> list, String path) {

        try {
            XMLEncoder encoder = new XMLEncoder(
                    new BufferedOutputStream(
                            new FileOutputStream(path)));

            encoder.setPersistenceDelegate(LocalDate.class,
                    new PersistenceDelegate() {
                        @Override
                        protected Expression instantiate(Object localDate, Encoder encdr) {
                            return new Expression(localDate,
                                    LocalDate.class,
                                    "parse",
                                    new Object[]{localDate.toString()});
                        }
                    });

            for (Bureau elem : list) {
                encoder.writeObject(elem);
                elem.saveListXML(encoder);
            }
            encoder.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public static My_linked_list<Bureau> read(String path) {
        try {
            boolean test = true;
            XMLDecoder decoder = new XMLDecoder(
                    new BufferedInputStream(
                            new FileInputStream(path)));
            My_linked_list<Bureau> list = new My_linked_list<>();
            while (test) {
                try {
                    Bureau object = (Bureau) decoder.readObject();
                    object.setRequirements((ArrayList<String>) decoder.readObject());
                    list.add(object);
                } catch (Exception e) {
                    test = false;
                }
            }
            return list;
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }


    public static <E extends Bureau> void ObjectWrite(My_linked_list<E> object, String path) throws IOException, ClassNotFoundException {
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(object);
        objectOutputStream.close();
    }

    @SuppressWarnings("unchecked")
    public static <E> My_linked_list<E> ObjectRead(My_linked_list<E> object, String path) throws IOException, ClassNotFoundException {
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            object = (My_linked_list<E>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            System.out.println("Error");
            System.err.println(e.getMessage());
        }
        return object;
    }
}
