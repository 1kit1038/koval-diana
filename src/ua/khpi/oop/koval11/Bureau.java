package ua.khpi.oop.koval11;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Bureau implements Serializable{
    private static final long serialVersionUID = 1L;
    private ArrayList<Person> person = new ArrayList<Person>();

    //------------------------------------------------------

    public void register(String gender, Date dateOfRegistration, String name, int age, String hobby, String requirements) {
        Person client = new Person();
        client.setRegistration_number(getRandomValue());
        client.setSex(gender);
        client.setDateOfRegistration(dateOfRegistration);
        client.setName(name);
        client.setAge(age);
        client.setHobby(hobby);
        client.setRequirements(requirements);
        this.person.add(client);
    }


    public void print() {
        for(int i = 0; i < this.person.size(); i++) {
            System.out.println("Number of registration: " + this.person.get(i).getRegistration_number());
            System.out.println("Gender: " + this.person.get(i).getSex());
            System.out.println("Date of registration: " + this.person.get(i).getDateOfRegistration());
            System.out.println("Name: " + this.person.get(i).getName());
            System.out.println("Age: " + this.person.get(i).getAge());
            System.out.println("Hobby: " + this.person.get(i).getHobby());
            System.out.println("Requirements: " + this.person.get(i).getRequirements() + "\n");
        }
    }

    public void setPerson(Person person) {
        this.person.add(person);
    }

    public void clearClients() {
        this.person.clear();
    }

    public Person getClientByIndex(int index){
        return this.person.get(index);
    }

    public int getSize() {
        return this.person.size();
    }

    private int getRandomValue() {
        int min = 0;
        int max = 99999999;
        int diff = max - min;
        Random random = new Random();
        int value = random.nextInt(diff + 1);
        return value += min;
    }
}