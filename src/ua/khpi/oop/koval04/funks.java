package ua.khpi.oop.koval04;

import java.util.Scanner;

public class funks {


    public static void print_menu()
    {
        System.out.println("1.Enter string");
        System.out.println("2.Show current string");
        System.out.println("3.Delete words according to the task");
        System.out.println("4.Show result string");
        System.out.println("5.Exit");
    }

    public static String input() {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter string: ");
        String str = in.nextLine();
        System.out.println("");
        //in.close();
        return str;
    }

    public static void print_string(String text) {
        System.out.println("Show current string:");
        System.out.println(text);
        System.out.println("");
    }

    static boolean Vowel(char c) {
        switch (c) {
            case 'A':
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                return true;
            default:
                return false;
        }
    }

    public static String FormatText(String text) {

        int strlenght = text.length();
        char b = ' ';
        StringBuilder s = new StringBuilder();

        int s1;
        int s2;

        m1:
        for (int i = 0; i < strlenght; i++) {
            char c = text.charAt(i);

            if (!(Vowel(c))) {
                s1 = i;
                while ((text.charAt(i) != ' ') && (i < strlenght)) {
                    i++;
                }
                s2 = i;
                if ((s2 - s1) == 6) {
                    continue m1;
                } else {
                    i = s1;
                    while ((text.charAt(i) != ' ') && (i < strlenght)) {
                        s.append(text.charAt(i));
                        i++;
                    }
                    s.append(' ');
                }
            } else {
                while ((text.charAt(i) != ' ') && (i < strlenght)) {
                    s.append(text.charAt(i));
                    i++;
                }
                s.append(' ');
            }

        }
        System.out.println("FormatText is done");
        System.out.println("");
        String outText = s.toString().trim();
        return outText;

    }

    public static void print_res_string(String outText) {
        System.out.print("Result string: ");
        System.out.println(outText);
        System.out.println("");
    }

    public static void info_about_author() {
        System.out.println("@Author: Koval Diana 1.KIT 103.8");
        System.out.println("@Date: 05.12.2019");
        System.out.println("@Task: Delete words of a given lenght which begin with a consonant letter");
        System.out.println("");
        System.out.println("Functions: " + '\n'
                + "1 - Enter string (Any string)" + '\n'
                + "2 - Show current string (From function 1)" + '\n'
                + "3 - Delete words according to the task (Execution of the delivered operation)" + '\n'
                + "4 - Show result string (New string after processing in function 3)" + '\n');
        System.out.println("");
    }

    public static void debug() {

        System.out.println("The program works successfully!");
        System.out.print("Original string is : ");
        System.out.println("Sergeo and Georgi can swimming and dancing ");
        System.out.print("String after processing : ");
        System.out.println(" and can swimming and dancing ");

    }
}
